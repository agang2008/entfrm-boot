package com.entfrm.toolkit.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.toolkit.entity.Dataset;

public interface DatasetService extends IService<Dataset> {

}
