package com.entfrm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.system.entity.Dict;

/**
 * <p>
 * 字典类型表 服务类
 * </p>
 *
 * @author entfrm
 * @since 2019-01-30
 */
public interface DictService extends IService<Dict> {

}
