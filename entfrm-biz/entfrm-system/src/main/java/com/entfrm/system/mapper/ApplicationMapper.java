package com.entfrm.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.system.entity.Application;

/**
 * @author entfrm
 * @date 2020-04-23 18:15:10
 * @description 应用Mapper接口
 */
public interface ApplicationMapper extends BaseMapper<Application> {

}
