package com.entfrm.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.system.entity.UserRole;

/**
 * <p>
 * 用户和角色关联表 Mapper 接口
 * </p>
 *
 * @author entfrm
 * @since 2019-01-30
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
