package com.entfrm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.system.entity.RoleMenu;

/**
 * <p>
 * 角色和菜单关联表 服务类
 * </p>
 *
 * @author entfrm
 * @since 2019-01-30
 */
public interface RoleMenuService extends IService<RoleMenu> {

}
