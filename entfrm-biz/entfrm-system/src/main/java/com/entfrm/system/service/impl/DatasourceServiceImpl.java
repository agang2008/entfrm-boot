package com.entfrm.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entfrm.system.entity.Datasource;
import com.entfrm.system.mapper.DatasourceMapper;
import com.entfrm.system.service.DatasourceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数据源 服务实现类
 * </p>
 *
 * @author entfrm
 * @since 2019-01-30
 */
@Service
public class DatasourceServiceImpl extends ServiceImpl<DatasourceMapper, Datasource> implements DatasourceService {

}
